import json
import pika


# connection = pika.BlockingConnection(
#     pika.ConnectionParameters('localhost', heartbeat=600, blocked_connection_timeout=300))
# channel = connection.channel()


def publish(method, body):
    connection = pika.BlockingConnection(
        pika.ConnectionParameters('localhost', heartbeat=600, blocked_connection_timeout=300))
    channel = connection.channel()

    properties = pika.BasicProperties(method)
    print(json.dumps(body))
    channel.basic_publish(exchange='', routing_key='likes', body=json.dumps(body), properties=properties)
